# Steps

## Step 1: Clone the GitHub repo
```
git clone https://github.com/wuxx/Colorlight-FPGA-Projects.git
```


## Step 2: Download ecpdap (or uses oss-cad)
```
https://github.com/adamgreig/ecpdap/releases
```


## Step 3: Get everything ready for use
```
> dmesg -w
```
![fig1](figs/fig1.png)

## Step 4: Verify that ecpdap can talk to the board
```
$ ecpdap probes
$ ecpdap scan
```
![fig2](figs/fig2.jpg)


## Step 5: Load a blink bitstream to the FPGA

```
$ cd ./Colorlight-FPGA-Projects/demo/i9
$ ecpdap program --freq 5000 blink.bit

```
![fig3](figs/fig3.jpg)
![fig4](figs/fig4.jpg)

Link: https://tomverbeure.github.io/2021/01/22/The-Colorlight-i5-as-FPGA-development-board.html



